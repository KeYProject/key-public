package de.uka.ilkd.key.logic;

import de.uka.ilkd.key.logic.sort.Sort;

public interface Sorted {

    public Sort sort();

}